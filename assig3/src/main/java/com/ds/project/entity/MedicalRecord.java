package com.ds.project.entity;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
@Transactional
public class MedicalRecord implements Serializable {
    private static final long serialVersionUID = -1892561327013038124L;

    private Long medicalRecordId;
    private String description;
    private List<Patient> patients;

    public Long getMedicalRecordId() {
        return medicalRecordId;
    }

    public void setMedicalRecordId(Long medicalRecordId) {
        this.medicalRecordId = medicalRecordId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
