package com.ds.project.entity;


import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDate;
@Transactional
public class Doctor implements Serializable {
    private static final long serialVersionUID = -1892561327013038124L;

    private Long doctorId;
    private String name;
    private LocalDate birthDate;
    private String adress;
    private Gender gender;
    private User user1;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }
}
