package com.ds.project.entity;


import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
@Transactional
public class Medication implements Serializable {
    private static final long serialVersionUID = -1892561327013038124L;


    private Long medicationId;
    private String name;
    private Double dosage;
    private List<SideEffect> sideEffects;

    public Long getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(Long medicationId) {
        this.medicationId = medicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDosage() {
        return dosage;
    }

    public void setDosage(Double dosage) {
        this.dosage = dosage;
    }

    public List<SideEffect> getSideEffect() {
        return sideEffects;
    }

    public void setSideEffect(List<SideEffect> sideEffects) {
        this.sideEffects = sideEffects;
    }
}
