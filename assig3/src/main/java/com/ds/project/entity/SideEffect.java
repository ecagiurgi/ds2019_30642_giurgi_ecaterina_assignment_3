package com.ds.project.entity;

import javax.transaction.Transactional;
import java.io.Serializable;

@Transactional
public class SideEffect implements Serializable {
    private static final long serialVersionUID = -1892561327013038124L;

    private Long sideEffectId;
    private String name;

    public Long getSideEffectId() {
        return sideEffectId;
    }

    public void setSideEffectId(Long sideEffectId) {
        this.sideEffectId = sideEffectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
