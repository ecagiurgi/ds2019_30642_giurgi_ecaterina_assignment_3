package com.ds.project.entity;


import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDate;
@Transactional
public class Caregiver implements Serializable {
    private static final long serialVersionUID = -1892561327013038124L;


    private Long id;
    private String name;
    private LocalDate birthDate;
    private String adress;
    private Gender gender;
    private User user2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }
}
