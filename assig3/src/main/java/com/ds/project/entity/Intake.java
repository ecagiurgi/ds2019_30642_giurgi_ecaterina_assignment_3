package com.ds.project.entity;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.sql.Timestamp;


@Transactional
public class Intake implements Serializable {
    private static final long serialVersionUID = -1892561327013038124L;

    private Long intakeId;
    private Timestamp startDate;
    private Timestamp endDate;
    private Double dosage;
    private Medication medication;
    private Patient patient;

    public Long getIntakeId() {
        return intakeId;
    }

    public void setIntakeId(Long intakeId) {
        this.intakeId = intakeId;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Double getDosage() {
        return dosage;
    }

    public void setDosage(Double dosage) {
        this.dosage = dosage;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
