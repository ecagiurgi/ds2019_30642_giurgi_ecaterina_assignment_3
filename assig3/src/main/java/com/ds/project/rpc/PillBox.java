package com.ds.project.rpc;

import com.ds.project.entity.Intake;
import java.util.List;

public interface PillBox {
    List<Intake> getMedicationPlan();
}

