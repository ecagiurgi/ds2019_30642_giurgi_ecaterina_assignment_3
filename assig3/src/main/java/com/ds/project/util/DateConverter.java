package com.ds.project.util;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class DateConverter {

    public static LocalDateTime convertTimeStampToLocalDateTime(Timestamp time){
        return time.toLocalDateTime();
    }

    public static Timestamp convertLocalDateTimeToTimeStamp(LocalDateTime date){
        return Timestamp.valueOf(date);
    }

}
