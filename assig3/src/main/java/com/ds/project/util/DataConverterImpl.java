package com.ds.project.util;


import com.ds.project.entity.Intake;

import java.util.List;

public class DataConverterImpl  {
    public static  Object[][] intakesToTableData(List<Intake> intakes) {
        Object[][] data = new Object[intakes.size()][6];
        for (int row = 0; row < data.length; row++) {
            data[row][0] = intakes.get(row).getIntakeId();
            data[row][1] = intakes.get(row).getMedication().getName();
            data[row][2] = intakes.get(row).getDosage();
            data[row][3] = intakes.get(row).getStartDate();
            data[row][4] = intakes.get(row).getEndDate();
            data[row][5] = intakes.get(row).getPatient().getName();
        }
        return data;
    }

    public  static  String[] intakesToTableColumnNames() {
        return new String[]{"Id", "Medication", "Dosage", "Start Date", "End Date", "Patient Name"};
    }

}
