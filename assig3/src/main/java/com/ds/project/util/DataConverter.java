package com.ds.project.util;


import com.ds.project.entity.Intake;

import java.util.List;

public interface DataConverter {

    Object[][] intakesToTableData(List<Intake> users);

    String[] intakesToTableColumnNames();

}
