package client;

import com.ds.project.entity.Intake;
import com.ds.project.util.DataConverterImpl;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

//@SpringBootApplication
public class ClientUI extends JFrame {

    private JFrame frame;
    private JTable table;
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private java.util.List<Intake> intakeList;

    public ClientUI(java.util.List<Intake> intakeList) throws HeadlessException, InterruptedException {
        this.intakeList = intakeList;
        initialize();


    }

    private void initialize() throws IllegalThreadStateException, InterruptedException {

        frame = new JFrame("Medical Plan");
        frame.setBounds(100, 100, 650, 435);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        Calendar now = Calendar.getInstance();
        JLabel timeLabel = new JLabel(dateFormat.format(now.getTime()));
        timeLabel.setFont(new Font("Helvetica Neue", Font.PLAIN, 18));
        timeLabel.setBounds(200, 1, 175, 25);
        timeLabel.setForeground(Color.red);
        frame.getContentPane().add(timeLabel);

        JLabel subtitleLable = new JLabel("Medication Plan for current patient: " + intakeList.get(0).getPatient().getName());
        subtitleLable.setFont(new Font("Helvetica Neue", Font.BOLD, 18));
        subtitleLable.setBounds(80, 30, 500, 25);
        frame.getContentPane().add(subtitleLable);


        JScrollPane scrollPane_3 = new JScrollPane();
        scrollPane_3.setBounds(50, 50, 550, 150);
        frame.getContentPane().add(scrollPane_3);

        JScrollPane scrollPane1 = new JScrollPane();
        scrollPane_3.setViewportView(scrollPane1);

        table = new JTable();
        scrollPane1.setViewportView(table);
        table.setVisible(true);
        java.util.List<Intake> newList = new ArrayList<>();
        for (Intake i : intakeList) {
            if ((now.getTime().after(Date.from(i.getStartDate().toInstant())) && (now.getTime().before(Date.from(i.getEndDate().toInstant()))))) {
                newList.add(i);
            }
            JButton btnTaken = new JButton("TAKEN");
            btnTaken.setBounds(50, 200 , 97, 25);
            btnTaken.setVisible(true);
            btnTaken.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    JOptionPane.showMessageDialog(frame.getContentPane(), "Selected medication was Taken!");

                }
            });
            frame.getContentPane().add(btnTaken);


        }
        table.setBackground(Color.pink);

         refreshTable(DataConverterImpl.intakesToTableData(newList), DataConverterImpl.intakesToTableColumnNames());



        new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Calendar now = Calendar.getInstance();
                timeLabel.setText(dateFormat.format(now.getTime()));
                timeLabel.setForeground(Color.red);
                timeLabel.setFont(new Font("Helvetica Neue", Font.PLAIN, 18));
                //btnTaken.setForeground(Color.black);
                for (Intake i : intakeList) {
                    int hour = now.get(Calendar.HOUR_OF_DAY);
                    System.out.println("acum: " + hour);
                    System.out.println("start: " +  i.getStartDate().getHours());
                    System.out.println("end: " + i.getEndDate().getHours());
                    if (hour > i.getStartDate().getHours() && hour < i.getEndDate().getHours()) {
                        System.out.println("caca");
                       // btnTaken.setVisible(true);

                    }
                }

            }
        }).start();

        if (!Thread.currentThread().isAlive())
            Thread.currentThread().start();

        frame.setVisible(true);
    }


        private void refreshTable (Object[][]data, String[]columnNames){
            DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
            tableModel.setDataVector(data, columnNames);
            tableModel.setColumnIdentifiers(columnNames);
            tableModel.fireTableDataChanged();
        }


    }

