package client;

import com.ds.project.entity.Intake;
import com.ds.project.rpc.PillBox;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

import javax.transaction.Transactional;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@Transactional
@SpringBootApplication
public class ClientRMI {

    @Bean
    RmiProxyFactoryBean rmiProxy() {
        RmiProxyFactoryBean bean = new RmiProxyFactoryBean();
        bean.setServiceInterface(PillBox.class);
        bean.setServiceUrl("rmi://localhost:1099/ceva");
        return bean;
    }

    public static void main(String[] args) throws HeadlessException, ClassNotFoundException {
//        PillBox pillBox = SpringApplication.run(ClientRMI.class, args)
//                .getBean(PillBox.class);
//
//
       // intakes = new ArrayList<>();
        System.setProperty("java.awt.headless", "false");
        ConfigurableApplicationContext context = new SpringApplicationBuilder(ClientRMI.class).headless(false).run(args);
        PillBox pillBox = context.getBean(PillBox.class);

        System.out.println("================Client Side ========================");
        System.out.println(pillBox.getMedicationPlan().get(1).getPatient().getPatientId());

        List<Intake> intakeList = pillBox.getMedicationPlan();
        System.setProperty("java.awt.headless", "false");
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    System.out.println(pillBox.getMedicationPlan().size());
                    ClientUI window = new ClientUI(pillBox.getMedicationPlan());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });



    }

}
