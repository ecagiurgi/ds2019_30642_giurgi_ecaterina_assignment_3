package com.ds.project.controller;

import com.ds.project.dto.PatientDTO;
import com.ds.project.dto.SideEffectDTO;
import com.ds.project.service.PatientService;
import com.ds.project.service.SideEffectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/sideEffect")
public class SideEffectRestController {

    private final SideEffectService sideEffectService;

    @Autowired
    public SideEffectRestController(SideEffectService sideEffectService) {
        this.sideEffectService = sideEffectService;
    }

    @GetMapping("/list")
    public List<SideEffectDTO> list() {
        return sideEffectService.findAll();
    }

    @GetMapping("/{id}")
    public SideEffectDTO findById(@PathVariable("id") Long id) {
        return sideEffectService.findById(id);
    }

    @PostMapping("/save")
    public SideEffectDTO save(@RequestBody SideEffectDTO sideEffectDTO) {
        return sideEffectService.save(sideEffectDTO);
    }

    @GetMapping("/delete/{id}")
    public SideEffectDTO delete(@PathVariable("id") Long id) {
        return sideEffectService.deleteSideEffect(id);
    }

}
