package com.ds.project.controller;


import com.ds.project.dto.DoctorDTO;
import com.ds.project.dto.LoginDTO;
import com.ds.project.dto.UserDTO;
import com.ds.project.entity.Login;
import com.ds.project.service.UserService;
import org.hibernate.type.UUIDBinaryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/user")
public class UserRestController {

    private final UserService userService;


    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/test")
    public String test() {
        return userService.test();
    }

    @GetMapping("/list")
    public List<UserDTO> list() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public UserDTO findByUserId(@PathVariable("id") UUID id) {
        return userService.findByUserId(id);
    }

    @PostMapping("/save")
    public UserDTO save(@RequestBody UserDTO userDTO) {
        return userService.save(userDTO);
    }

//    @PostMapping("/chgpsw")
//    public UserDTO changePassword(@RequestBody ChangePassword changePassword) {
//        return userService.changePassword(changePassword.getUsername(),changePassword.getPassword(),changePassword.getNewPassword());
//    }

    @PostMapping("/login")
    public UserDTO login(@RequestBody LoginDTO loginDTO) {
        return userService.login(loginDTO);
    }

    @GetMapping("/delete/{id}")
    public UserDTO delete(@PathVariable("id") UUID id) {
        return userService.deleteUser(id);
    }
}
