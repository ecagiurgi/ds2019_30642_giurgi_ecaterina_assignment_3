package com.ds.project.controller;

import com.ds.project.dto.MedicationDTO;
import com.ds.project.dto.PatientDTO;
import com.ds.project.service.MedicationService;
import com.ds.project.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/medication")
public class MedicationRestController {
    private final MedicationService medicationService;

    @Autowired
    public MedicationRestController( MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping("/list")
    public List<MedicationDTO> list() {
        return medicationService.findAll();
    }

    @GetMapping("/{id}")
    public MedicationDTO findById(@PathVariable("id") Long id) {
        return medicationService.findById(id);
    }

    @PostMapping("/save")
    public MedicationDTO save(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.save(medicationDTO);
    }

    @GetMapping("/delete/{id}")
    public MedicationDTO delete(@PathVariable("id") Long id) {
        return medicationService.deleteMedication(id);
    }

}
