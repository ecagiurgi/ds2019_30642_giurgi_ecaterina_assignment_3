package com.ds.project.service.impl;

import com.ds.project.dto.PatientDTO;
import com.ds.project.entity.Patient;
import com.ds.project.mapper.PatientMapper;
import com.ds.project.repository.PatientRepository;
import com.ds.project.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PatientServiceImpl implements PatientService {
    private final PatientMapper patientMapper;
    private final PatientRepository patientRepository;


    @Autowired
    public PatientServiceImpl(PatientMapper patientMapper,
                             PatientRepository patientRepository) {
        this.patientMapper = patientMapper;
        this.patientRepository = patientRepository;
    }


    @Override
    public PatientDTO save(PatientDTO patientDto) {
        Patient patient;
        if (patientDto.getId() != null) {
            patient = patientRepository.findByPatientId(patientDto.getId());
        } else {
            patient = new Patient();
        }
        patientMapper.toEntityUpdate(patient, patientDto);
        PatientDTO patientDTO1 = patientMapper.toDTO(patientRepository.save(patient));
        return patientDTO1;    }

    @Override
    public List<PatientDTO> findAll() {
        return patientRepository.findAll()
                .stream()
                .map(patientMapper::toDTO)
                .collect(Collectors.toList());        }

    @Override
    public PatientDTO findById(Long id) {
            return patientMapper.toDTO(patientRepository.findByPatientId(id));    }

    @Override
    public PatientDTO deletePatient(Long id) {
        patientRepository.deleteById(id);
        return null;    }
}
