package com.ds.project.service;

import com.ds.project.dto.IntakeDTO;

import java.util.List;

public interface IntakeService {
    IntakeDTO save(IntakeDTO intakeDTO);

    List<IntakeDTO> findAll();

    IntakeDTO findById(Long id);

    IntakeDTO deleteIntake(Long id);
}
