package com.ds.project.service;


import com.ds.project.dto.PatientDTO;

import java.util.List;

public interface PatientService {
    PatientDTO save(PatientDTO patientDto);

    List<PatientDTO> findAll();

    PatientDTO findById(Long id);

    PatientDTO deletePatient(Long id);


}
