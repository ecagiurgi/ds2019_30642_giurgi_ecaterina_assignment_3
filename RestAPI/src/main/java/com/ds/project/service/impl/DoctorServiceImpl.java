package com.ds.project.service.impl;

import com.ds.project.dto.DoctorDTO;
import com.ds.project.entity.Doctor;
import com.ds.project.mapper.DoctorMapper;
import com.ds.project.repository.DoctorRepository;
import com.ds.project.service.DoctorSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorServiceImpl implements DoctorSerivce {

    private final DoctorMapper doctorMapper;
    private final DoctorRepository doctorRepository;


    @Autowired
    public DoctorServiceImpl( DoctorMapper doctorMapper,
                            DoctorRepository doctorRepository) {
        this.doctorMapper = doctorMapper;
        this.doctorRepository = doctorRepository;
    }

    @Override
    public DoctorDTO save(DoctorDTO doctorDto) {
        Doctor doctor;
        if (doctorDto.getDoctorId() != null) {
            doctor = doctorRepository.findByDoctorId(doctorDto.getDoctorId());
        } else {
            //  userDto.setType(T.USER);
            doctor = new Doctor();
        }
        doctorMapper.toEntityUpdate(doctor, doctorDto);
        DoctorDTO doctorDTO1 = doctorMapper.toDTO(doctorRepository.save(doctor));
//        Order order = new Order();
//        order.setUser(user);
//        orderRepository.save(order);
//        System.out.println(order);
        return doctorDTO1;    }

    @Override
    public List<DoctorDTO> findAll() {
        return doctorRepository.findAll()
                .stream()
                .map(doctorMapper::toDTO)
                .collect(Collectors.toList());    }

    @Override
    public DoctorDTO findById(Long id) {
        return doctorMapper.toDTO(doctorRepository.findByDoctorId(id));
    }

    @Override
    public void deleteDoctor(Long id){
        Doctor doctor;
        doctor = doctorRepository.findByDoctorId(id);
        doctorRepository.delete(doctor);
    }
}
