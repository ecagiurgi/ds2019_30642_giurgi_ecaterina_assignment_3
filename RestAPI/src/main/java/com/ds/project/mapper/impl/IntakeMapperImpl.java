package com.ds.project.mapper.impl;

import com.ds.project.dto.IntakeDTO;
import com.ds.project.entity.Intake;
import com.ds.project.mapper.IntakeMapper;
import com.ds.project.mapper.MedicationMapper;
import com.ds.project.mapper.PatientMapper;
import org.springframework.stereotype.Component;

@Component
public class IntakeMapperImpl implements IntakeMapper {

    MedicationMapper medicationMapper = new MedicationMapperImpl();
    PatientMapper patientMapper = new PatientMapperImpl();

    @Override
    public IntakeDTO toDTO(Intake entity) {
        if (entity == null) {
            return null;
        }

        IntakeDTO intakeDTO = new IntakeDTO();

        intakeDTO.setIntakeId(entity.getIntakeId());
        intakeDTO.setStartDate(entity.getStartDate());
        intakeDTO.setEndDate(entity.getEndDate());
        intakeDTO.setDosage(entity.getDosage());
        intakeDTO.setMedicationDTO(medicationMapper.toDTO(entity.getMedication()));
        intakeDTO.setPatientDTO(patientMapper.toDTO(entity.getPatient()));

        return intakeDTO;
    }

    @Override
    public Intake toEntity(IntakeDTO dto) {

        if ( dto == null ) {
            return null;
        }

        Intake intake = new Intake();

        intake.setIntakeId( dto.getIntakeId() );
        intake.setStartDate( dto.getStartDate());
        intake.setEndDate(dto.getEndDate());
        intake.setDosage(dto.getDosage());
        intake.setMedication(medicationMapper.toEntity(dto.getMedicationDTO()));
        intake.setPatient(patientMapper.toEntity(dto.getPatientDTO()));

        return intake;
    }

    @Override
    public void toEntityUpdate(Intake intake, IntakeDTO intakeDTO) {
        if ( intakeDTO == null ) {
            return ;
        }
        intake.setIntakeId( intakeDTO.getIntakeId() );
        intake.setStartDate( intakeDTO.getStartDate());
        intake.setEndDate(intakeDTO.getEndDate());
        intake.setDosage(intakeDTO.getDosage());
        intake.setMedication(medicationMapper.toEntity(intakeDTO.getMedicationDTO()));
        intake.setPatient(patientMapper.toEntity(intakeDTO.getPatientDTO()));

    }


}
