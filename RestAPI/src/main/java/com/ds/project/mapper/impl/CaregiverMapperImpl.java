package com.ds.project.mapper.impl;

import com.ds.project.dto.CaregiverDTO;
import com.ds.project.entity.Caregiver;
import com.ds.project.mapper.CaregiverMapper;
import com.ds.project.mapper.UserMapper;
import org.springframework.stereotype.Component;

@Component
public class CaregiverMapperImpl implements CaregiverMapper {
    UserMapper userMapper = new UserMapperImpl();

    @Override
    public CaregiverDTO toDTO(Caregiver entity) {
        if (entity == null) {
            return null;
        }

        CaregiverDTO caregiverDTO = new CaregiverDTO();

        caregiverDTO.setId(entity.getId());
        caregiverDTO.setName(entity.getName());
        caregiverDTO.setBirthDate(entity.getBirthDate());
        caregiverDTO.setAdress(entity.getAdress());
        caregiverDTO.setGender(entity.getGender());
        caregiverDTO.setUser2DTO(userMapper.toDTO(entity.getUser2()));

        return caregiverDTO;
    }

    @Override
    public Caregiver toEntity(CaregiverDTO dto) {
        if (dto == null) {
            return null;
        }

        Caregiver caregiver = new Caregiver();

        caregiver.setId(dto.getId());
        caregiver.setName(dto.getName());
        caregiver.setBirthDate(dto.getBirthDate());
        caregiver.setAdress(dto.getAdress());
        caregiver.setGender(dto.getGender());
        caregiver.setUser2(userMapper.toEntity(dto.getUser2DTO()));

        return caregiver;
    }

    @Override
    public void toEntityUpdate(Caregiver caregiver, CaregiverDTO caregiverDTO) {
        if (caregiverDTO == null) {
            return;
        }
        caregiver.setId(caregiverDTO.getId());
        caregiver.setName(caregiverDTO.getName());
        caregiver.setBirthDate(caregiverDTO.getBirthDate());
        caregiver.setAdress(caregiverDTO.getAdress());
        caregiver.setGender(caregiverDTO.getGender());
        caregiver.setUser2(userMapper.toEntity(caregiverDTO.getUser2DTO()));

    }
}
