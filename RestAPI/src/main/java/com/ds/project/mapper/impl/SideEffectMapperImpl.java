package com.ds.project.mapper.impl;

import com.ds.project.dto.SideEffectDTO;
import com.ds.project.entity.SideEffect;
import com.ds.project.mapper.SideEffectMapper;
import org.springframework.stereotype.Component;

@Component
public class SideEffectMapperImpl implements SideEffectMapper {
    @Override
    public SideEffectDTO toDTO(SideEffect entity) {
        if (entity == null) {
            return null;
        }

        SideEffectDTO sideEffectDTO = new SideEffectDTO();

        sideEffectDTO.setSideEffectId(entity.getSideEffectId());
        sideEffectDTO.setName(entity.getName());
        return sideEffectDTO;
    }

    @Override
    public SideEffect toEntity(SideEffectDTO dto) {
        if (dto == null) {
            return null;
        }

        SideEffect sideEffect = new SideEffect();

        sideEffect.setSideEffectId(dto.getSideEffectId());
        sideEffect.setName(dto.getName());
        return sideEffect;
    }

    @Override
    public void toEntityUpdate(SideEffect sideEffect, SideEffectDTO sideEffectDTO) {
        if (sideEffectDTO == null) {
            return;
        }
        sideEffect.setSideEffectId(sideEffectDTO.getSideEffectId());
        sideEffect.setName(sideEffectDTO.getName());
    }
}
