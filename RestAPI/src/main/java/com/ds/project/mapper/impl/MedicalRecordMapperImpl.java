package com.ds.project.mapper.impl;

import com.ds.project.dto.MedicalRecordDTO;
import com.ds.project.dto.PatientDTO;
import com.ds.project.entity.MedicalRecord;
import com.ds.project.entity.Patient;
import com.ds.project.mapper.MedicalRecordMapper;
import com.ds.project.mapper.PatientMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MedicalRecordMapperImpl implements MedicalRecordMapper {

    PatientMapper patientMapper = new PatientMapperImpl();

    @Override
    public MedicalRecordDTO toDTO(MedicalRecord entity) {
        if (entity == null) {
            return null;
        }

        MedicalRecordDTO medicalRecordDTO = new MedicalRecordDTO();

        medicalRecordDTO.setMedicalRecordId(entity.getMedicalRecordId());
        medicalRecordDTO.setDescription(entity.getDescription());
        medicalRecordDTO.setPatientsDto(patientsListToPatientsDTOList(entity.getPatients()));


        return medicalRecordDTO;
    }

    @Override
    public MedicalRecord toEntity(MedicalRecordDTO dto) {
        if ( dto == null ) {
            return null;
        }

        MedicalRecord medicalRecord = new MedicalRecord();

        medicalRecord.setMedicalRecordId( dto.getMedicalRecordId() );
        medicalRecord.setDescription( dto.getDescription());
        medicalRecord.setPatients(patientsDTOListToPatientsList(dto.getPatientsDto()));

        return medicalRecord;
    }

    @Override
    public void toEntityUpdate(MedicalRecord medicalRecord, MedicalRecordDTO medicalRecordDTO) {
        if (medicalRecordDTO == null) {
            return;
        }
        medicalRecord.setMedicalRecordId( medicalRecordDTO.getMedicalRecordId() );
        medicalRecord.setDescription( medicalRecordDTO.getDescription());
        medicalRecord.setPatients(patientsDTOListToPatientsList(medicalRecordDTO.getPatientsDto()));
    }

    public List<PatientDTO> patientsListToPatientsDTOList(List<Patient> list) {
        if ( list == null ) {
            return null;
        }

        List<PatientDTO> list1 = new ArrayList<PatientDTO>( list.size() );
        for ( Patient i : list ) {
            list1.add( patientMapper.toDTO( i ) );
        }

        return list1;
    }

    public List<Patient> patientsDTOListToPatientsList(List<PatientDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<Patient> list1 = new ArrayList<Patient>( list.size() );
        for ( PatientDTO i : list ) {
            list1.add( patientMapper.toEntity( i ) );
        }

        return list1;
    }
}
