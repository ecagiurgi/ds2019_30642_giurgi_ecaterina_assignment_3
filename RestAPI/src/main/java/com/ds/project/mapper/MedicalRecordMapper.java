package com.ds.project.mapper;

import com.ds.project.dto.MedicalRecordDTO;
import com.ds.project.entity.MedicalRecord;

public interface MedicalRecordMapper {


    MedicalRecordDTO toDTO(MedicalRecord entity);
    MedicalRecord toEntity(MedicalRecordDTO dto);
    void toEntityUpdate(MedicalRecord medicalRecord, MedicalRecordDTO medicalRecordDTO);
}
