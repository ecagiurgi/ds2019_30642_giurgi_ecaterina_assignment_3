package com.ds.project.mapper.impl;

import com.ds.project.dto.MedicationDTO;
import com.ds.project.dto.SideEffectDTO;
import com.ds.project.entity.Medication;
import com.ds.project.entity.SideEffect;
import com.ds.project.mapper.MedicationMapper;
import com.ds.project.mapper.SideEffectMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MedicationMapperImpl implements MedicationMapper {

    SideEffectMapper sideEffectMapper = new SideEffectMapperImpl();

    @Override
    public MedicationDTO toDTO(Medication entity) {
        if (entity == null) {
            return null;
        }

        MedicationDTO medicationDTO = new MedicationDTO();

        medicationDTO.setId(entity.getId());
        medicationDTO.setName(entity.getName());
        medicationDTO.setDosage(entity.getDosage());
        medicationDTO.setSideEffectDTOS(sideEffectListToSideEffectDTOList(entity.getSideEffects()));


        return medicationDTO;
    }

    @Override
    public Medication toEntity(MedicationDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Medication medication = new Medication();

        medication.setId( dto.getId() );
        medication.setName( dto.getName() );
        medication.setDosage( dto.getDosage() );
        medication.setSideEffects(sideEffectDTOListToSideEffectList(dto.getSideEffectDTOS()));

        return medication;     }

    @Override
    public void toEntityUpdate(Medication medication, MedicationDTO medicationDTO) {
        if (medicationDTO == null) {
            return;
        }
        medication.setId(medicationDTO.getId());
        medication.setName(medicationDTO.getName());
        medication.setDosage(medicationDTO.getDosage());
        medication.setSideEffects(sideEffectDTOListToSideEffectList(medicationDTO.getSideEffectDTOS()));
    }

    public List<SideEffectDTO> sideEffectListToSideEffectDTOList(List<SideEffect> list) {
        if ( list == null ) {
            return null;
        }

        List<SideEffectDTO> list1 = new ArrayList<SideEffectDTO>( list.size() );
        for ( SideEffect i : list ) {
            list1.add( sideEffectMapper.toDTO( i ) );
        }

        return list1;
    }

    public List<SideEffect> sideEffectDTOListToSideEffectList(List<SideEffectDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<SideEffect> list1 = new ArrayList<SideEffect>( list.size() );
        for ( SideEffectDTO i : list ) {
            list1.add( sideEffectMapper.toEntity( i ) );
        }

        return list1;
    }



}
