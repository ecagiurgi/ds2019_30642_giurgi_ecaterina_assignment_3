package com.ds.project.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long medicationId;
    private String name;
    private Double dosage;

    @ManyToMany(cascade = CascadeType.DETACH)
    private List<SideEffect> sideEffects =new ArrayList<>();

    public Medication(){}

    public Long getId() {
        return medicationId;
    }

    public void setId(Long id) {
        this.medicationId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDosage() {
        return dosage;
    }

    public void setDosage(Double dosage) {
        this.dosage = dosage;
    }

    public List<SideEffect> getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(List<SideEffect> sideEffects) {
        this.sideEffects = sideEffects;
    }
}
