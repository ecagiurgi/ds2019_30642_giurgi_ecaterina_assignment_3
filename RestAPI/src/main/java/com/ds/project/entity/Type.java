package com.ds.project.entity;

public enum Type {
    DOCTOR,
    PATIENT,
    CAREGIVER
}
