package com.ds.project.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "caregiver")
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = "user_id", name = "fk_user2")}
)
public class Caregiver {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long caregiverId;
    private String name;
    private LocalDate birthDate;
    private String adress;

    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "user_id")
    private User user2;

    public Caregiver(String name, LocalDate birthDate, String adress, Gender gender) {
        this.name = name;
        this.birthDate = birthDate;
        this.adress = adress;
        this.gender = gender;
    }

    public Caregiver() {
    }

    public Long getId() {
        return caregiverId;
    }

    public void setId(Long id) {
        this.caregiverId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }
}
