package com.ds.project.dto;

import java.util.List;

public class MedicalRecordDTO {
    private Long medicalRecordId;
    private String description;
    private List<PatientDTO> patientsDto;

    public Long getMedicalRecordId() {
        return medicalRecordId;
    }

    public void setMedicalRecordId(Long medicalRecordId) {
        this.medicalRecordId = medicalRecordId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PatientDTO> getPatientsDto() {
        return patientsDto;
    }

    public void setPatientsDto(List<PatientDTO> patientsDto) {
        this.patientsDto = patientsDto;
    }
}
