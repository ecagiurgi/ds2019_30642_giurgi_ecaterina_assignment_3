package com.ds.project.dto;

import com.ds.project.entity.Medication;
import com.ds.project.entity.Patient;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

public class IntakeDTO {
    private Long intakeId;
    private LocalDate startDate;
    private LocalDate endDate;
    private Double dosage;
    private MedicationDTO medicationDTO;
    private PatientDTO patientDTO;

    public Long getIntakeId() {
        return intakeId;
    }

    public void setIntakeId(Long intakeId) {
        this.intakeId = intakeId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Double getDosage() {
        return dosage;
    }

    public void setDosage(Double dosage) {
        this.dosage = dosage;
    }

    public MedicationDTO getMedicationDTO() {
        return medicationDTO;
    }

    public void setMedicationDTO(MedicationDTO medicationDTO) {
        this.medicationDTO = medicationDTO;
    }

    public PatientDTO getPatientDTO() {
        return patientDTO;
    }

    public void setPatientDTO(PatientDTO patientDTO) {
        this.patientDTO = patientDTO;
    }
}
