package com.ds.project.dto;

import com.ds.project.entity.SideEffect;

import java.util.List;

public class MedicationDTO {

    private Long id;
    private String name;
    private Double dosage;
    private List<SideEffectDTO> sideEffectDTOS;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDosage() {
        return dosage;
    }

    public void setDosage(Double dosage) {
        this.dosage = dosage;
    }

    public List<SideEffectDTO> getSideEffectDTOS() {
        return sideEffectDTOS;
    }

    public void setSideEffectDTOS(List<SideEffectDTO> sideEffectDTOS) {
        this.sideEffectDTOS = sideEffectDTOS;
    }
}
