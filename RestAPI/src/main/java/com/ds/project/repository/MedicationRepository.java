package com.ds.project.repository;

import com.ds.project.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long> {
    Medication findByMedicationId(final Long id);
}
