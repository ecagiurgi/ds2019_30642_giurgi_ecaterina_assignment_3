package com.ds.project.repository;

import com.ds.project.entity.User;
import org.hibernate.type.UUIDBinaryType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    User findByUserId(final UUID  id);
    User findByUsername(final String username);
}
